> PigX 系列视频  

- [pigx 环境准备、运行pigx](https://www.bilibili.com/video/av33200189) | [PPT](https://slides.com/lengleng/pigx-springcloud-6/fullscreen#/)   
- [pigx OAuth2 调用原理、源码解析（必看）](https://www.bilibili.com/video/av20229859/?p=3) 
- [pigx 定时任务、运维平台使用](https://www.bilibili.com/video/av33268288) | [PPT](https://slides.com/lengleng/pigx-springcloud-7/fullscreen#/)   
- [pigx 配置文件加解密原理和选择](https://www.bilibili.com/video/av33315412) | [PPT](https://slides.com/lengleng/pigx-springcloud-6-8/fullscreen#/)   
- [pigx 终端接口调用](https://www.bilibili.com/video/av33926330) | [PPT](https://slides.com/lengleng/pigx-springcloud-7-9/fullscreen#/)   
- [pigX 二次开发整合业务微服务](https://www.bilibili.com/video/av35170377) | [PPT](https://slides.com/lengleng/pigx-springcloud-7-9-12)  
- [pigX 分布式事务详解](https://www.bilibili.com/video/av35170266) | [PPT](http://www.txlcn.org/v4/ppt/LCN4.0.pptx)  
- [pigx docker compose + nginx部署](https://www.bilibili.com/video/av34744931) | [PPT](https://slides.com/lengleng/pigx-springcloud-6-8-11/fullscreen#/)  
- [pigx K8S部署(一)](https://www.bilibili.com/video/av35128044) | [资料](https://gitbook.cn/m/mazi/activity/5b728973a281bb5781de5f18)  

